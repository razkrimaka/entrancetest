﻿using System;
using UnityEngine;

namespace Model.Properties
{
    [Serializable]
    public class SpritePropertyProvider : BasePropertyProvider<Sprite>
    {
        [SerializeField]
        private Sprite _value;

        protected override Sprite InternalValue => _value;
    }
}