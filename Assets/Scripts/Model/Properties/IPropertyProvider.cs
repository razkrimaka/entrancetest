﻿namespace Model.Properties
{
    public interface IPropertyProvider
    {
        uint Id { get; }
    }

    public interface IPropertyProvider<T> : IPropertyProvider
    {
        T Value { get; }

        void Edit(T value);
    }
}