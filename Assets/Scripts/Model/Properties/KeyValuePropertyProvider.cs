﻿using System;
using UnityEngine;

namespace Model.Properties
{
    [Serializable]
    public abstract class KeyValuePropertyProvider<TKey, TValue> : BasePropertyProvider<string>
    {
        [SerializeField]
        protected TKey _key;

        [SerializeField]
        protected TValue _value;
    }

    [Serializable]
    public class StringIntPropertyProvider : KeyValuePropertyProvider<string, int>
    {
        protected override string InternalValue => $"{_key} : {_value}";
    }
    
    [Serializable]
    public class StringStringPropertyProvider : KeyValuePropertyProvider<string, string>
    {
        protected override string InternalValue => $"{_key} : {_value}";
    }
}