﻿using System;
using UnityEngine;

namespace Model.Properties
{
    [Serializable]
    public class IntPropertyProvider : BasePropertyProvider<int>
    {
        [SerializeField]
        private int _value;

        protected override int InternalValue => _value;
    }
}