﻿namespace Model.Properties
{
    public class RuntimeProperty<T> : IPropertyProvider<T>
    {
        public RuntimeProperty(uint id, T value)
        {
            Id = id;
            Value = value;
        }
        
        private uint Id { get; set; }
        private T Value { get; set; }

        #region IPropertyProvider

        uint IPropertyProvider.Id => Id;
        T IPropertyProvider<T>.Value => Value;

        void IPropertyProvider<T>.Edit(T value)
        {
            Value = value;
        }

        #endregion
    }
}