﻿using System;
using UnityEngine;

namespace Model.Properties
{
    [Serializable]
    public abstract class BasePropertyProvider<T> : IPropertyProvider<T>
    {
        [SerializeField]
        private uint _propertyId;

        public T Value => InternalValue;
        
        protected abstract T InternalValue { get; }

        #region IPropertyProvider
        
        uint IPropertyProvider.Id => _propertyId;

        void IPropertyProvider<T>.Edit(T value)
        {
            Debug.LogError("Cannot edit serializable property");
        }
        
        #endregion
    }
}