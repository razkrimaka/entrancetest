﻿using System;
using UnityEngine;

namespace Model.Properties
{
    [Serializable]
    public class StringPropertyProvider : BasePropertyProvider<string>
    {
        [SerializeField]
        private string _value;

        protected override string InternalValue => _value;
    }
}