﻿using System;
using System.Collections.Generic;
using Model.Properties;
using UnityEngine;
using UnityEngine.Assertions;

namespace Model.Database
{
    public class RuntimeDatabaseItem : IDatabaseItem
    {
        private readonly uint _id;
        private readonly Dictionary<uint, IPropertyProvider> _propertyProviders;
        
        public RuntimeDatabaseItem(uint id, IEnumerable<IPropertyProvider> propertyProviders)
        {
            _id = id;
            _propertyProviders = new Dictionary<uint, IPropertyProvider>();

            foreach (var propertyProvider in propertyProviders)
            {
                Assert.IsFalse(_propertyProviders.ContainsKey(propertyProvider.Id));
                
                _propertyProviders[propertyProvider.Id] = CreateProviderFromOther(propertyProvider);
            }
        }

        private IPropertyProvider CreateProviderFromOther(IPropertyProvider source)
        {
            return source switch
            {
                IPropertyProvider<int> intPropertyProvider => new RuntimeProperty<int>(intPropertyProvider.Id, intPropertyProvider.Value),
                IPropertyProvider<Sprite> spritePropertyProvider => new RuntimeProperty<Sprite>(spritePropertyProvider.Id, spritePropertyProvider.Value),
                IPropertyProvider<string> stringPropertyProvider => new RuntimeProperty<string>(stringPropertyProvider.Id, stringPropertyProvider.Value),
                _ => throw new Exception($"Cannot create runtimeProperty {source.GetType()}")
            };
        }

        #region IDatabaseItem

        uint IDatabaseItem.Id => _id;
        IEnumerable<IPropertyProvider> IDatabaseItem.PropertyProviders => _propertyProviders.Values;
        
        void IDatabaseItem.Edit(uint propertyId, IPropertyProvider value)
        {
            _propertyProviders[propertyId] = CreateProviderFromOther(value);
        }

        #endregion
    }
}