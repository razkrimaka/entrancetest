﻿using System.Collections.Generic;
using Model.Properties;

namespace Model.Database
{
    public interface IDatabase
    {
        IEnumerable<IDatabaseItem> Items { get; }
    }

    public interface IEditableDatabase
    {
        void Edit(uint objectId, uint propertyId, IPropertyProvider propertyProvider);
        void Add(uint objectId, uint propertyId, IPropertyProvider propertyProvider);
        void Remove(uint objectId);
    }
}