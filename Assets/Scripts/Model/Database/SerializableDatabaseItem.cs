﻿using System.Collections.Generic;
using Model.Properties;
using UnityEngine;

namespace Model.Database
{
    [CreateAssetMenu(fileName = nameof(SerializableDatabaseItem), menuName = "EntranceTest/" + nameof(SerializableDatabaseItem))]
    public class SerializableDatabaseItem : ScriptableObject, IDatabaseItem
    {
        [SerializeField]
        private uint _id;

        [SerializeReference]
        private List<IPropertyProvider> _propertyProviders;

        #region IDatabaseItem

        uint IDatabaseItem.Id => _id;

        IEnumerable<IPropertyProvider> IDatabaseItem.PropertyProviders => _propertyProviders;

        void IDatabaseItem.Edit(uint propertyId, IPropertyProvider value)
        {
            Debug.LogError($"Cannot edit serializable database item");
        }

        #endregion
    }
}