﻿using System.Collections.Generic;
using Model.Properties;

namespace Model.Database
{
    public interface IDatabaseItem
    {
        uint Id { get; }
        IEnumerable<IPropertyProvider> PropertyProviders { get; }

        void Edit(uint propertyId, IPropertyProvider value);
    }
}