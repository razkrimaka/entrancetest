﻿using System.Collections.Generic;
using Model.Properties;
using UnityEngine;
using UnityEngine.Assertions;

namespace Model.Database
{
    [CreateAssetMenu(fileName = nameof(SerializableDatabase), menuName = "EntranceTest/" + nameof(SerializableDatabase))]
    public class SerializableDatabase : ScriptableObject, IDatabase
    {
        [SerializeField]
        private List<SerializableDatabaseItem> _items;

        #region IDatabase

        IEnumerable<IDatabaseItem> IDatabase.Items => _items;

        #endregion
    }
}