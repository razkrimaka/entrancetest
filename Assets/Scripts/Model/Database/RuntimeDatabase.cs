﻿using System;
using System.Collections.Generic;
using Model.Properties;
using UnityEngine.Assertions;

namespace Model.Database
{
    public class RuntimeDatabase : IDatabase, IEditableDatabase
    {
        public RuntimeDatabase(IDatabase database)
        {
            _cachedItems = new Dictionary<uint, IDatabaseItem>();
            
            foreach (var databaseItem in database.Items)
            {
                Assert.IsFalse(_cachedItems.ContainsKey(databaseItem.Id));

                _cachedItems[databaseItem.Id] = new RuntimeDatabaseItem(databaseItem.Id, databaseItem.PropertyProviders);
            }
        }

        private Dictionary<uint, IDatabaseItem> _cachedItems;
        #region IDatabase

        IEnumerable<IDatabaseItem> IDatabase.Items => _cachedItems.Values;

        void IEditableDatabase.Add(uint objectId, uint propertyId, IPropertyProvider propertyProvider)
        {
            if (!_cachedItems.TryGetValue(objectId, out var databaseItem))
            {
                databaseItem = new RuntimeDatabaseItem(objectId, new[] {propertyProvider});
                _cachedItems.Add(objectId, databaseItem);
            }

            databaseItem.Edit(propertyId, propertyProvider);
        }

        void IEditableDatabase.Remove(uint objectId)
        {
            if (_cachedItems.ContainsKey(objectId))
            {
                _cachedItems.Remove(objectId);
                return;
            }

            throw new Exception($"Object with id {objectId} not exist");
        }

        void IEditableDatabase.Edit(uint objectId, uint propertyId, IPropertyProvider propertyProvider)
        {
            if (!_cachedItems.TryGetValue(objectId, out var databaseItem))
            {
                throw new Exception($"Cannot find item with id {objectId}");
            }
            
            databaseItem.Edit(propertyId, propertyProvider);
        }

        #endregion
    }
}