﻿using System;
using Common.Factory;
using Model.Properties;
using UnityEngine;
using View.PropertyViews;

namespace View
{
    public class PropertyViewFactory : IFactory<IPropertyProvider, IPropertyView>
    {
        private readonly IPropertyView<string> _stringViewPrefab;
        private readonly IPropertyView<int> _intViewPrefab;
        private readonly IPropertyView<Sprite> _spriteViewPrefab;

        private Transform _parent;

        public PropertyViewFactory(IPropertyView<string> stringViewPrefab,
            IPropertyView<int> intViewPrefab,
            IPropertyView<Sprite> spriteViewPrefab)
        {
            _stringViewPrefab = stringViewPrefab;
            _intViewPrefab = intViewPrefab;
            _spriteViewPrefab = spriteViewPrefab;
        }

        public void SetParent(Transform parent)
        {
            _parent = parent;
        }

        private IPropertyView CreatePropertyView<T>(IPropertyProvider<T> propertyProvider, IPropertyView<T> viewTemplate)
        {
            var instance = viewTemplate.CreateInstance(_parent);
            instance.SetPropertyValue(propertyProvider.Value);

            return instance;
        }

        #region IFactory

        public IPropertyView Create(IPropertyProvider input)
        {
            if (_parent == default)
            {
                throw new Exception($"Need initialize parent before use {nameof(PropertyViewFactory)}");
            }
            
            var view = input switch
            {
                IPropertyProvider<int> intPropertyProvider => CreatePropertyView(intPropertyProvider, _intViewPrefab),
                IPropertyProvider<Sprite> spritePropertyProvider => CreatePropertyView(spritePropertyProvider, _spriteViewPrefab),
                IPropertyProvider<string> stringPropertyProvider => CreatePropertyView(stringPropertyProvider, _stringViewPrefab),
                _ => throw new Exception($"Cannot create view for {input.GetType()}")
            };

            return view;
        }

        #endregion
    }
}