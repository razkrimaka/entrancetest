﻿using UnityEngine;

namespace View
{
    public class DatabaseObjectView : MonoBehaviour
    {
        [SerializeField]
        private Transform _propertiesContainer;

        public Transform PropertiesContainer => _propertiesContainer;
    }
}