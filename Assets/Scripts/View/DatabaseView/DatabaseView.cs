﻿using System;
using Common.Commands;
using Common.Factory;
using Model.Database;
using Runtime;
using UnityEngine;
using View.PropertyViews;

namespace View
{
    public class DatabaseModel : IViewModel
    {
        public Command ShowEditCommand;
    }

    public class DatabaseView : BaseView<DatabaseModel>
    {
        [Header("Templates")]
        [SerializeField]
        private DatabaseObjectView _databaseObjectViewPrefab;

        [SerializeField]
        private StringPropertyView _stringPropertyViewPrefab;

        [SerializeField]
        private IntPropertyView _intPropertyViewPrefab;

        [SerializeField]
        private SpritePropertyView _spritePropertyViewPrefab;

        [Header("Containers")]
        [SerializeField]
        private Transform _objectsContainer;

        protected override void OnShow()
        {
            var propertyViewFactory = new PropertyViewFactory(_stringPropertyViewPrefab, _intPropertyViewPrefab, _spritePropertyViewPrefab);
            IFactory<IDatabaseItem, DatabaseObjectView> databaseObjectFactory =
                new DatabaseObjectFactory(_databaseObjectViewPrefab, propertyViewFactory, _objectsContainer);

            var database = Context.Database;

            foreach (var databaseItem in database.Items)
            {
                databaseObjectFactory.Create(databaseItem);
            }
        }

        public void OnClickEdit()
        {
            Model.ShowEditCommand.Execute();
        }
    }
}