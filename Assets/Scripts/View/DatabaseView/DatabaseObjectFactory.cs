﻿using System.Collections.Generic;
using System.Linq;
using Common.Factory;
using Model.Database;
using Model.Properties;
using UnityEngine;

namespace View
{
    public class DatabaseObjectFactory : IFactory<IDatabaseItem, DatabaseObjectView>
    {
        private readonly DatabaseObjectView _prefab;
        private readonly PropertyViewFactory _propertyViewFactory;
        private readonly Transform _parent;

        private static List<IPropertyProvider> _cache;
        
        public DatabaseObjectFactory(DatabaseObjectView prefab, PropertyViewFactory propertyViewFactory, Transform parent)
        {
            _prefab = prefab;
            _propertyViewFactory = propertyViewFactory;
            _parent = parent;
        }
        
        #region IFactory

        DatabaseObjectView IFactory<IDatabaseItem, DatabaseObjectView>.Create(IDatabaseItem input)
        {
            var instance = Object.Instantiate(_prefab, _parent);
            _propertyViewFactory.SetParent(instance.PropertiesContainer);

            _cache = input.PropertyProviders.ToList();
            _cache.Sort((a, b) =>
                a.Id > b.Id ? 1 :
                a.Id == b.Id ? 0 : -1);

            foreach (var propertyProvider in _cache)
            {
                _propertyViewFactory.Create(propertyProvider);
            }
            
            _cache.Clear();

            return instance;
        }

        #endregion
    }
}