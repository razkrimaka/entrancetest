﻿using Common.Commands;

namespace View
{
    public class MainModel : IViewModel
    {
        public Command ShowDatabaseCommand;
    }
    
    public class MainView : BaseView<MainModel>
    {
        public void OnClickShowDatabase()
        {
            Model.ShowDatabaseCommand.Execute();
        }
    }
}