﻿using System;
using Model.Properties;
using TMPro;
using UnityEngine;

namespace View
{
    public class SpritePropertyCreator : IPropertyCreator
    {
        private readonly TMP_InputField _idField;
        private readonly Func<Sprite> _spriteGetter;

        public SpritePropertyCreator(TMP_InputField idField, Func<Sprite> spriteGetter)
        {
            _idField = idField;
            _spriteGetter = spriteGetter;
        }

        #region IPropertyCreator

        IPropertyProvider IPropertyCreator.Create()
        {
            if (!uint.TryParse(_idField.text, out var id))
            {
                throw new Exception("Cannot parse field id");
            }

            var propertyProvider = new RuntimeProperty<Sprite>(id, _spriteGetter?.Invoke());

            return propertyProvider;
        }

        #endregion
    }
}