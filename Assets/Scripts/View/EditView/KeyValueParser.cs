﻿using System;
using Model.Properties;
using TMPro;

namespace View
{
    public class KeyValueParser : IPropertyCreator
    {
        private readonly TMP_InputField _idField;
        private readonly TMP_InputField _valueField;
        private readonly TMP_InputField _keyField;

        public KeyValueParser(TMP_InputField idField, TMP_InputField keyField, TMP_InputField valueField)
        {
            _idField = idField;
            _keyField = keyField;
            _valueField = valueField;
        }

        #region IPropertyCreator

        IPropertyProvider IPropertyCreator.Create()
        {
            if (!uint.TryParse(_idField.text, out var id))
            {
                throw new Exception("Cannot parse field id");
            }

            var propertyProvider = new RuntimeProperty<string>(id, $"{_keyField.text} : {_valueField.text}");

            return propertyProvider;
        }

        #endregion
    }
}