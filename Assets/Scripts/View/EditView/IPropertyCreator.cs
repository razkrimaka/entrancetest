﻿using Model.Properties;

namespace View
{
    public interface IPropertyCreator
    {
        IPropertyProvider Create();
    }
}