﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Model.Properties;
using TMPro;
using UnityEngine;

namespace View
{
    public class EditModel : IViewModel
    {
        public Action<(uint Id, IPropertyProvider PropertyProvider)> OnEdit;
        public Action<(uint Id, IPropertyProvider PropertyProvider)> OnAdd;
        public Action<uint> OnRemove;
    }
    
    public class EditView : BaseView<EditModel>
    {
        [SerializeField]
        private TMP_InputField _objectId;

        [SerializeField]
        private TMP_InputField _propertyId;

        [SerializeField]
        private TMP_InputField _newValueField;

        [SerializeField]
        private TMP_InputField _newKeyValueField;

        [SerializeField]
        private ExtendedToggle[] _toggles;

        [SerializeField]
        private ExtendedToggle[] _spriteToggles;

        [SerializeField]
        private SpriteSerializableData[] _spriteDatas;

        [SerializeField]
        private GameObject _spriteTogglesContainer;

        private Dictionary<string, IPropertyCreator> _propertyCreators;

        private IPropertyCreator _currentPropertyCreator;
        private Sprite _selectedSprite;

        protected override void OnShow()
        {
            _propertyCreators = new Dictionary<string, IPropertyCreator>()
            {
                {"int", new PropertyParser(_propertyId, _newValueField, PropertyParser.ParseStrategy.Integer)},
                {"string", new PropertyParser(_propertyId, _newValueField, PropertyParser.ParseStrategy.String)},
                {"KeyValue", new KeyValueParser(_propertyId, _newKeyValueField, _newValueField)},
                {"Sprite", new SpritePropertyCreator(_propertyId, () => _selectedSprite)}
            };

            foreach (var toggle in _toggles)
            {
                toggle.Toggle.onValueChanged.AddListener((isOn) => OnToggleValueChanged(toggle.Id, isOn));
            }

            foreach (var spriteToggle in _spriteToggles)
            {
                spriteToggle.Toggle.onValueChanged.AddListener((isOn) => OnSpriteToggleValueChanged(spriteToggle.Id, isOn));
            }
        }

        private void OnSpriteToggleValueChanged(string id, bool isOn)
        {
            if (!isOn)
            {
                return;
            }

            _selectedSprite = _spriteDatas.FirstOrDefault(data => data.Id == id).Sprite;
        }

        private void OnToggleValueChanged(string id, bool isOn)
        {
            if (!isOn)
            {
                return;
            }

            if (!_propertyCreators.TryGetValue(id, out _currentPropertyCreator))
            {
                Debug.LogError($"Cannot find creator for toggle: {id}");
            }

            switch (id)
            {
                case "KeyValue":
                    _newKeyValueField.gameObject.SetActive(true);
                    _spriteTogglesContainer.SetActive(false);
                    _newValueField.gameObject.SetActive(true);
                    break;
                case "Sprite":
                    _newKeyValueField.gameObject.SetActive(false);
                    _spriteTogglesContainer.SetActive(true);
                    _newValueField.gameObject.SetActive(false);
                    break;
                default:
                    _newKeyValueField.gameObject.SetActive(false);
                    _spriteTogglesContainer.SetActive(false);
                    _newValueField.gameObject.SetActive(true);
                    break;
            }
        }

        public void OnEditClick()
        {
            if (!uint.TryParse(_objectId.text, out var objectId))
            {
                Debug.LogError($"Cannot parse object with id: {_objectId.text}");
                return;
            }

            var property = _currentPropertyCreator?.Create();

            Model.OnEdit?.Invoke((objectId, property));
        }

        public void OnAddClick()
        {
            if (!uint.TryParse(_objectId.text, out var objectId))
            {
                Debug.LogError($"Cannot parse object with id: {_objectId.text}");
                return;
            }
            
            var property = _currentPropertyCreator?.Create();

            Model.OnAdd?.Invoke((objectId, property));
        }

        public void OnRemoveClick()
        {
            if (!uint.TryParse(_objectId.text, out var objectId))
            {
                Debug.LogError($"Cannot parse object with id: {_objectId.text}");
                return;
            }

            Model?.OnRemove(objectId);
        }
        
        [Serializable]
        private struct SpriteSerializableData
        {
            [SerializeField]
            private string _id;

            [SerializeField]
            private Sprite _sprite;

            public string Id => _id;
            public Sprite Sprite => _sprite;
        }
    }
}