﻿using System;
using Model.Properties;
using TMPro;

namespace View
{
    public class PropertyParser : IPropertyCreator
    {
        private readonly TMP_InputField _idField;
        private readonly TMP_InputField _valueField;
        private readonly ParseStrategy _parseStrategy;
        
        public PropertyParser(TMP_InputField idField, TMP_InputField valueField, ParseStrategy parseStrategy)
        {
            _idField = idField;
            _valueField = valueField;
            _parseStrategy = parseStrategy;
        }
        
        #region IPropertyCreator

        IPropertyProvider IPropertyCreator.Create()
        {
            if (!uint.TryParse(_idField.text, out var id))
            {
                throw new Exception("Cannot parse field id");
            }

            IPropertyProvider result;

            switch (_parseStrategy)
            {
                case ParseStrategy.Integer:
                    if (!int.TryParse(_valueField.text, out var value))
                    {
                        throw new Exception("Cannot parse");
                    }
                    result = new RuntimeProperty<int>(id, value);
                    break;
                case ParseStrategy.String:
                    result = new RuntimeProperty<string>(id, _valueField.text);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return result;
        }

        #endregion

        public enum ParseStrategy
        {
            Integer,
            String
        }
    }
}