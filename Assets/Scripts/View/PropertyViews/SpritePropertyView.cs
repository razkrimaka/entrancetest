﻿using UnityEngine;
using UnityEngine.UI;

namespace View.PropertyViews
{
    [RequireComponent(typeof(Image))]
    public class SpritePropertyView : BasePropertyView<Sprite>
    {
        [SerializeField]
        private Image _image;
        
        #region IPropertyView

        public override void SetPropertyValue(Sprite value)
        {
            _image.sprite = value;
        }

        #endregion
    }
}