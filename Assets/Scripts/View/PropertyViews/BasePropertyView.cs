﻿using UnityEngine;

namespace View.PropertyViews
{
    public abstract class BasePropertyView<T> : MonoBehaviour, IPropertyView<T>
    {
        public abstract void SetPropertyValue(T value);

        public IPropertyView<T> CreateInstance(Transform parent)
        {
            return Instantiate(gameObject, parent)
                .GetComponent<IPropertyView<T>>();
        }
    }
}