﻿using TMPro;
using UnityEngine;

namespace View.PropertyViews
{
    public class IntPropertyView : BasePropertyView<int>
    {
        [SerializeField]
        private TMP_Text _valueText;
        
        #region IPropertyView

        public override void SetPropertyValue(int value)
        {
            _valueText.text = $"{value}";
        }

        #endregion
    }
}