﻿using UnityEngine;

namespace View.PropertyViews
{
    public interface IPropertyView
    {
    }
    
    public interface IPropertyView<in T> : IPropertyView
    {
        void SetPropertyValue(T value);
        
        IPropertyView<T> CreateInstance(Transform parent);
    }
}