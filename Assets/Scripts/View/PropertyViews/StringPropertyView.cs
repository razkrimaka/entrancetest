﻿using TMPro;
using UnityEngine;

namespace View.PropertyViews
{
    public class StringPropertyView : BasePropertyView<string>
    {
        [SerializeField]
        private TMP_Text _valueText;
        
        #region IPropertyView

        public override void SetPropertyValue(string value)
        {
            _valueText.text = $"{value}";
        }

        #endregion
    }
}