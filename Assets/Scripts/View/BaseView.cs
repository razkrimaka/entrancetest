﻿using System;
using UnityEngine;

namespace View
{
    public abstract class BaseView<T> : MonoBehaviour, IView<T>
    where T: IViewModel
    {
        protected T Model { get; private set; }

        private static BaseView<T> _instance;
        
        public void Show(T model, Transform parent)
        {
            if (_instance != null)
            {
                throw new Exception($"View already exist");
            }
            _instance = Instantiate(this, parent);
            _instance.Model = model;
            _instance.OnShow();
        }

        public void Hide()
        {
            if (_instance == null)
            {
                throw new Exception($"View not exist");
            }
            
            _instance.OnHide();
            Destroy(_instance.gameObject);
        }

        protected virtual void OnShow(){}
        protected virtual void OnHide(){}
    }
}