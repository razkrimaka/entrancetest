﻿using UnityEngine;

namespace View
{
    public interface IView<in T> : IView
        where T : IViewModel
    {
        void Show(T model, Transform parent);
    }

    public interface IView
    {
        void Hide();
    }
}