﻿using Common.Commands;
using Common.UI;
using Model.Database;
using UnityEngine;
using View;

namespace Runtime
{
    public class GameEntry : MonoBehaviour
    {
        [SerializeField]
        private SerializableDatabase _serializableDatabase;

        [SerializeField]
        private DatabaseView _databaseViewPrefab;

        [SerializeField]
        private EditView _editViewPrefab;

        [SerializeField]
        private MainView _mainView;

        [SerializeField]
        private Transform _uiRoot;

        public void Start()
        {
            var ui = new Ui(_uiRoot);
            var runtimeDatabase = new RuntimeDatabase(_serializableDatabase);
            Context.ContextConfiguration.Initialize(runtimeDatabase, ui);
            
            Context.Ui.AddWindow(_databaseViewPrefab);
            Context.Ui.AddWindow(_editViewPrefab);
            Context.Ui.AddWindow(_mainView);
            
            Context.Ui.Show(new MainModel()
            {
                ShowDatabaseCommand = new ShowDatabaseCommand()
            });
        }
    }
}