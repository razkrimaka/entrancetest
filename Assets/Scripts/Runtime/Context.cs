﻿using System;
using Common.UI;
using Model.Database;

namespace Runtime
{
    public static class Context
    {
        public static IDatabase Database { get; private set; }
        public static IEditableDatabase EditableDatabase { get; private set; }
        public static IUi Ui { get; private set; }

        public static class ContextConfiguration
        {
            private static bool _initialized;

            public static void Initialize(RuntimeDatabase database, IUi ui)
            {
                if (_initialized)
                {
                    throw new Exception($"Context now initialized. Call {nameof(Release)}");
                }
                Database = database;
                EditableDatabase = database;
                Ui = ui;

                _initialized = true;
            }

            public static void Release()
            {
                _initialized = false;

                Database = default;
                EditableDatabase = default;
                Ui = default;
            }
        }
    }
}