﻿using Model.Properties;
using Runtime;
using View;

namespace Common.Commands
{
    public class ShowEditCommand : Command
    {
        public override void Execute()
        {
            var model = new EditModel()
            {
                OnEdit = OnEdit,
                OnAdd = OnAdd,
                OnRemove = OnRemove
            };
            
            Context.Ui.Show(model);
        }

        private void OnEdit((uint Id, IPropertyProvider PropertyProvider) changes)
        {
            Context.EditableDatabase.Edit(changes.Id, changes.PropertyProvider.Id, changes.PropertyProvider);
            ShowDatabase();
        }

        private void OnAdd((uint Id, IPropertyProvider PropertyProvider) changes)
        {
            Context.EditableDatabase.Add(changes.Id, changes.PropertyProvider.Id, changes.PropertyProvider);
            ShowDatabase();
        }

        private void OnRemove(uint objectId)
        {
            Context.EditableDatabase.Remove(objectId);
            ShowDatabase();
        }

        private void ShowDatabase()
        {
            var model = new DatabaseModel()
            {
                ShowEditCommand = new ShowEditCommand()
            };
            
            Context.Ui.Show(model);
        }
    }
}