﻿using Runtime;
using View;

namespace Common.Commands
{
    public class ShowDatabaseCommand : Command
    {
        public override void Execute()
        {
            var model = new DatabaseModel()
            {
                ShowEditCommand = new ShowEditCommand()
            };
            
            Context.Ui.Show(model);
        }
    }
}