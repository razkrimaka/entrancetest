﻿namespace Common.Commands
{
    public abstract class Command
    {
        public abstract void Execute();
    }
}