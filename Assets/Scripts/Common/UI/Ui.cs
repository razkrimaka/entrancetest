﻿using System;
using System.Collections.Generic;
using UnityEngine;
using View;

namespace Common.UI
{
    public class Ui : IUi
    {
        private Dictionary<Type, IView> _views = new Dictionary<Type, IView>();
        private readonly Transform _parent;
        private IView _currentView;
        
        public Ui(Transform parent)
        {
            _parent = parent;
        }
        
        #region IUi

        void IUi.Show<T>(T model)
        {
            if (_currentView != default)
            {
                _currentView.Hide();
            }
            
            if (_views.TryGetValue(typeof(T), out var view) && view is IView<T> derivedView)
            {
                derivedView.Show(model, _parent);
                _currentView = derivedView;
            }
        }

        void IUi.AddWindow<T>(IView<T> window)
        {
            _views[typeof(T)] = window;
        }

        #endregion
    }
}