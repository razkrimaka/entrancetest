﻿using View;

namespace Common.UI
{
    public interface IUi
    {
        void Show<T>(T model) where T : IViewModel;
        void AddWindow<T>(IView<T> window) where T : IViewModel;
    }
}