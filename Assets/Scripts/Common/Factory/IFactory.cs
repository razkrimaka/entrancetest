﻿namespace Common.Factory
{
    public interface IFactory<in TInput, out TOutput>
    {
        TOutput Create(TInput input);
    }
}