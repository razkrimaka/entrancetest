﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace Common
{
    [RequireComponent(typeof(Toggle))]
    public class ExtendedToggle : MonoBehaviour
    {
        [SerializeField]
        private string _id;

        [ReadOnly]
        [SerializeField]
        private Toggle _toggle;

        public Toggle Toggle => _toggle;
        public string Id => _id;

        public void Reset()
        {
            _toggle = GetComponent<Toggle>();
        }
    }
}